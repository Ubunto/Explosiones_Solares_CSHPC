#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from __future__ import print_function

# $example on$
from numpy import array
import numpy as np

# $example off$

from pyspark import SparkContext
# $example on$
from pyspark.mllib.clustering import GaussianMixture, GaussianMixtureModel
# $example off$
from time import time, strftime

def parseDataset(lines):
    rdd = lines.map(lambda line: array([float(x) for x in line.split(' ')]))
    return rdd

def generate_probabilities(points, k, model, count_lines):
    probabilities = np.zeros(k)
    points = points.map(lambda point: (model.predict(point), 1.0)) \
                    .reduceByKey(lambda p1, p2: p1 + p2) \
                    .sortByKey() \
                    .collect()

    for p in points:
                probabilities[p[0]] = float(p[1]) / count_lines
    return probabilities



if __name__ == "__main__":
    sc = SparkContext(appName="GaussianMixtureExample")  # SparkContext

    # $example on$
    # Load and parse the data
    sameModel = GaussianMixtureModel.load(sc, "hdfs://masterNode:9000/user/spark/output/kmeans_felipe/GaussianMixtureModel")
    predict_data = sc.textFile("hdfs://masterNode:9000/user/spark/dataset_k_means/Pruebas_2013_2016.txt")
    #parsedData = data.map(lambda line: array([float(x) for x in line.strip().split(' ')]))
    #points = data.map(lambda line: array([float(x) for x in line.strip().split(' ')]))
    # Build the model (cluster the data)
    #gmm = GaussianMixture.train(parsedData, 10 , maxIterations=20)

    # Save and load model
    #gmm.save(sc, "hdfs://masterNode:9000/user/spark/output/kmeans_felipe/GaussianMixtureModel")
    #sameModel = GaussianMixtureModel\
    #   .load(sc, "hdfs://masterNode:9000/user/spark/output/kmeans_felipe/GaussianMixtureModel")

    start = time()
    # Predicting
    points = parseDataset(predict_data)
    count_lines = float(len(points.collect()))
    probabilities = generate_probabilities(points, 10, sameModel, count_lines)
    print("Prob: ", probabilities)

    end = time()
    elapsed_time = end - start

    sc.stop()
    # output parameters of model
   # for i in range(1):
       # print("weight = ", gmm.weights[i], "mu = ", gmm.gaussians[i].mu,
             # "sigma = ", gmm.gaussians[i].sigma.toArray())
    # $example off$
