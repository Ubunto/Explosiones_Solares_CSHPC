from __future__ import print_function

# $example on$
from numpy import array
from math import sqrt
# $example off$

from pyspark import SparkContext
# $example on$
from pyspark.mllib.clustering import KMeans, KMeansModel
# $example off$

sc = SparkContext(appName="KMeansExample") # SparkContext

# Load and parse the data
data = sc.textFile("hdfs://masterNode:9000/user/spark/dataset_k_means/entrenamiento_2014_2015.txt")
parsedData = data.map(lambda line: array([float(x) for x in line.split(' ')]))

# Build the model (cluster the data)
clusters = KMeans.train(parsedData, 10, maxIterations=55,
                        runs=15, initializationMode="random")

# Evaluate clustering by computing Within Set Sum of Squared Errors
def error(point):
    center = clusters.centers[clusters.predict(point)]
    return sqrt(sum([x**2 for x in (point - center)]))

WSSSE = parsedData.map(lambda point: error(point)).reduce(lambda x, y: x + y)
print("Within Set Sum of Squared Error = " + str(WSSSE))

# Save and load model
clusters.save(sc, "hdfs://masterNode:9000/user/spark/output/kmeans_felipe/KMeansModel")
#sameModel = KMeansModel.load(sc, "hdfs://masterNode:9000/user/spark/output/kmeans_felipe/KMeansModel")
