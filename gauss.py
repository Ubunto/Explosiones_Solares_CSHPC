#
# Licensed to the Apache Softwa0re Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from __future__ import print_function

# $example on$
from numpy import array
# $example off$

from pyspark import SparkContext
# $example on$
from pyspark.mllib.clustering import GaussianMixture, GaussianMixtureModel
# $example off$

if __name__ == "__main__":
    sc = SparkContext(appName="GaussianMixtureExample")  # SparkContext

    # $example on$
    # Load and parse the data
    data = sc.textFile("hdfs://masterNode:9000/user/spark/dataset_k_means/Pruebas_2013_2016.txt")
    #parsedData = data.map(lambda line: array([float(x) for x in line.strip().split(' ')]))
    points = data.map(lambda line: array([float(x) for x in line.strip().split(' ')]))
    # Build the model (cluster the data)
    #gmm = GaussianMixture.train(parsedData, 10 , maxIterations=20)

    # Save and load model
    #gmm.save(sc, "hdfs://masterNode:9000/user/spark/output/kmeans_felipe/GaussianMixtureModel")
    sameModel = GaussianMixtureModel\
        .load(sc, "hdfs://masterNode:9000/user/spark/output/kmeans_felipe/GaussianMixtureModel")

    k = 10
    count_lines = len(points.collect())
    list_pro = np.zeros(k)
    points.foreach(lambda point: list_pro[sameModel.predict(point)] = list_pro[sameModel.predict(point)] +1)
    list_pro = map(lambda data: data/float(count_lines), list_pro)
    print(list_pro)
    # output parameters of model
   # for i in range(1):
       # print("weight = ", gmm.weights[i], "mu = ", gmm.gaussians[i].mu,
             # "sigma = ", gmm.gaussians[i].sigma.toArray())
    # $example off$

sc.stop()
