#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <sstream>
#include <string>
#include <cstring>
#include <stdlib.h> 
#include <string>
#include <chrono>
#include <ctime>
#include <cuda.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
using namespace std;

#define PI 3.14159265358979323846
#define TILE_WIDTH 16

//Funcion para separar las fechas
void tokenizar(string fecha , vector<int> &numeros);

//Funcion para leer los archivos
void leer_archivo(string nombre_archivo, float **m, vector<string> &semanas , int dia , int mes , int ano);

//Funcion para pasar de int a string
void int_tostring(int dia, int mes, int ano, vector<string> &semanas);

//Funcion para calcular todas las semanas entre 2 fechas
void calcular_semanas(vector<string> &semanas , int dia , int mes , int ano , int dia_termino , int mes_termino, int ano_termino );


int signo(float x);
void Copiar(float **a, float **m1 , int n);
void valoresPropios(float *valores, int maxIter , int n , float **m1 , float **p_normal);
float absoluto(float x);



__global__ void matrixMultiply(float *A, float *B, float *C, int numARows,
	int numAColumns, int numBRows, int numBColumns,
	int numCRows, int numCColumns) {
	__shared__ float ds_M[TILE_WIDTH][TILE_WIDTH];
	__shared__ float ds_N[TILE_WIDTH][TILE_WIDTH];

	int bx = blockIdx.x;
	int by = blockIdx.y;
	int tx = threadIdx.x;
	int ty = threadIdx.y;

	int Row = by * TILE_WIDTH + ty;
	int Col = bx * TILE_WIDTH + tx;
	float Pvalue = 0;
// ciclo sobre las matrices "shared" M y N  para calcular el
// elemento P
	for (int m = 0; m < (numAColumns - 1) / TILE_WIDTH + 1; ++m) {
// Se verifica que tanto como tx como ty, no excedan el tamaño de la
// matrices, y si lo llegaran a hacer por el tamaño del grid, estos se
// asignaran como 0
		if (Row < numARows && ((m * TILE_WIDTH) + tx) < numAColumns) {
			ds_M[ty][tx] = A[Row * numAColumns + m * TILE_WIDTH + tx];

		} else {
			ds_M[ty][tx] = 0;
		}
		if ((Col < numBColumns) && ((m * TILE_WIDTH) + ty) < numBRows) {
			ds_N[ty][tx] = B[(m * TILE_WIDTH + ty) * numBColumns + Col];

		} else {
			ds_N[ty][tx] = 0.0;
		}
		__syncthreads();
		for (int k = 0; k < TILE_WIDTH; ++k)
			Pvalue += ds_M[ty][k] * ds_N[k][tx];
		__syncthreads();
	}
// Solo se guardaran si hilos corresponden a una posicion valida para la
// matriz resultante
	if (Row < numCRows && Col < numCColumns) {
		C[Row * numCColumns + Col] = Pvalue;
	}
}
void matMultiplyOnHost(float *A, float *B, float *C, int numARows,
	int numAColumns, int numBRows, int numBColumns,
	int numCRows, int numCColumns) {
	for (int i = 0; i < numARows; i++) {
		for (int j = 0; j < numBColumns; j++) {
			float result = 0.0;
			for (int k = 0; k < numAColumns; k++) {
				result += A[i * numAColumns + k] * B[k * numBColumns + j];
			}
			C[i * numBColumns + j] = result;
		}
	}
}
void Check(float *m_h, float *m_d, int numCRows, int numCColumns) {
	for (int i = 0; i < numCRows * numCColumns; i++) {
		if (m_h[i] != m_d[i]) {
			cout << "Iqual: False" << endl;
			break;
		}
	}
	cout << "Iqual: True" << endl;
}


int main(){

	vector<string> semanas;

	calcular_semanas(semanas, 22 , 4 , 2013 , 28 , 5 , 2016);


	float *hostA;
	float *valor_propio;  
	float *hostB; 
	float *hostC; 
	float *deviceA = NULL;
	float *deviceB = NULL;
	float *deviceC = NULL;
	int numrowA;
	int numcolsA;
	int numrowA_Traspuesta;
	int numcolsA_Traspuesta;


	numrowA = semanas.size()-1;
	numcolsA = (17292*7);

	numrowA_Traspuesta = (17292*7);
	numcolsA_Traspuesta = semanas.size()-1;

	int numCrows = numrowA;
	int numCColumns = numcolsA_Traspuesta;

	float sizeA = sizeof(float) * (numrowA * numcolsA);
	float sizeB = sizeof(float) * (numrowA_Traspuesta * numcolsA_Traspuesta);
	float sizeC = sizeof(float) * numCrows * numCColumns;
	float sizeD = sizeof(float) * numrowA;


	hostA = (float *)malloc(sizeA);
	valor_propio = (float *)malloc(sizeD);
	hostB = (float *)malloc(sizeB);
	hostC = (float *)malloc(sizeC);

	float **m;

	m = new float * [numrowA];

	for(int i = 0; i < numrowA; i++)
		m[i] = new float [numcolsA];


	for(int i = 0 ; i < numrowA ; i++){
		for(int j = 0 ; j < numcolsA ; j++){
			m[i][j] = 0.0;
		}
	}


	vector<string> nombre_archivo;

	string archivo;

	ifstream datosRuta("listado.txt");

	int contador2 = 0;
	while(!datosRuta.eof()){

		string linea;
		getline(datosRuta,linea);
		nombre_archivo.push_back(linea);
	}
	datosRuta.close();


	for(int i = 0 ; i < nombre_archivo.size() ; i++){
		string str2 = nombre_archivo[i].substr (8,10);
		vector<int> fecha;
		tokenizar(str2 , fecha);
		string fichero_inicial = "Archivos/";	
		string nombre_final = fichero_inicial+nombre_archivo[i];
		leer_archivo(nombre_final , m , semanas , fecha[2] , fecha[1] , fecha[0]);
	}

	int contador32 = 0;
	for(int i = 0 ; i < numrowA ; i++){
		for(int j = 0 ; j < numcolsA ; j++){

			hostA[contador32] = m[i][j];
			contador32++;
		}
	}

	contador32 = 0;
	for(int i = 0 ; i < numcolsA ; i++){
		for(int j = 0 ; j < numrowA ; j++){

			hostB[contador32] = m[j][i];
			contador32++;	
		}
	}


// Memoria en device

	cudaMalloc((void **)&deviceA, sizeA);
	cudaMalloc((void **)&deviceB, sizeB);
	cudaMalloc((void **)&deviceC, sizeC);

// Host to Device
	cudaMemcpy(deviceA, hostA, sizeA, cudaMemcpyHostToDevice);
	cudaMemcpy(deviceB, hostB, sizeB, cudaMemcpyHostToDevice);

	dim3 dimGrid((numCColumns - 1) / TILE_WIDTH + 1,
		(numCrows - 1) / TILE_WIDTH + 1, 1);
	dim3 dimBlock(TILE_WIDTH, TILE_WIDTH, 1);

// Multiplicacion de matrices utilizando tiles en device
	matrixMultiply<<<dimGrid, dimBlock>>>(deviceA, deviceB, deviceC, numrowA,
		numcolsA, numrowA_Traspuesta, numcolsA_Traspuesta,
		numCrows, numCColumns);

// Device to Host
	cudaMemcpy(hostC, deviceC, sizeC, cudaMemcpyDeviceToHost);


	cudaFree(deviceA);
	cudaFree(deviceB);
	cudaFree(deviceC);


	float **mfinal;

	mfinal = new float * [numrowA];

	for(int i = 0; i < numrowA; i++)
		mfinal[i] = new float [numrowA];


	for(int i = 0 ; i < numrowA ; i++){
		for(int j = 0 ; j < numrowA ; j++){
			mfinal[i][j] = 0.0;
		}
		
	}


	//Normal
	for(int i = 0 ; i < numrowA ; i++){
		for(int j = 0 ; j < numrowA ; j++){
			
			if(hostC[i*numrowA+j] == 0){
				mfinal[i][j] = hostC[i*numrowA+j];
			}else{
				mfinal[i][j] = hostC[i*numrowA+j]/1000000;	
			}
		}
	}
	ofstream salida("datos_salida_final.txt");

	
	for(int i = 0 ; i < numrowA ; i++){
		for(int j = 0 ; j < numrowA ; j++){
			//salida << hostC[i*numrowA+j] << " ";
			salida << mfinal[i][j] << " ";
		}
		salida << endl;
	}


	float **p_normal;

	p_normal = new float * [numrowA];

	for(int i = 0; i < numrowA; i++)
		p_normal[i] = new float [numrowA];


	
	cout << "Termine de multiplicar bien" << endl;

	valoresPropios(valor_propio , 100000 , numrowA , mfinal , p_normal);


	return 0;
}


void calcular_semanas(vector<string> &semanas , int dia , int mes , int ano , int dia_termino , int mes_termino, int ano_termino ){

	map<int,int> meses;
	meses[1] = 31;
	meses[2] = 28;
	meses[3] = 31;
	meses[4] = 30;
	meses[5] = 31;
	meses[6] = 30;
	meses[7] = 31;
	meses[8] = 31;
	meses[9] = 30;
	meses[10] = 31;
	meses[11] = 30;
	meses[12] = 31;

	int contador_semana = 0;

	int_tostring(dia,mes,ano,semanas);

	while(mes < mes_termino+1 || ano != ano_termino){

		dia = dia+7;

		if(mes == 2){
			if(ano%4 == 0){
				meses[mes] = 29;
			}else{
				meses[mes] = 28;
			}
		}

		if(meses[mes] > dia){
			contador_semana++;

		}
		else if(meses[mes] < dia){
			contador_semana++;
			dia = dia-meses[mes];

			if(mes == 12){
				mes = 1;
				ano++;
			}else{
				mes++;
			}
		}

		if(mes < mes_termino+1 || ano != ano_termino){
			int_tostring(dia,mes,ano,semanas);
		}

	}

}

void tokenizar(string fecha , vector<int> &numeros){

	string palabra;
	palabra = "";

	for(int i = 0; i < fecha.size() ; i++){

		if(fecha[i] == '-'){
			int num = atoi(palabra.c_str());
			numeros.push_back(num);
			palabra = "";
		}else{
			palabra += fecha[i];
		}

	}
	int num = atoi(palabra.c_str());
	numeros.push_back(num);

}


void leer_archivo(string nombre_archivo, float **m ,vector<string> &semanas, int dia , int mes , int ano){

	map<int,int> meses;
	meses[1] = 31;
	meses[2] = 28;
	meses[3] = 31;
	meses[4] = 30;
	meses[5] = 31;
	meses[6] = 30;
	meses[7] = 31;
	meses[8] = 31;
	meses[9] = 30;
	meses[10] = 31;
	meses[11] = 30;
	meses[12] = 31;


//obtengo donde tiene que ir
	bool encontrado = false;
	int numero_guardado;

	int dia_inicio;
	int mes_inicio;
	int ano_inicio;

	int dia_termino;
	int mes_termino;
	int ano_termino;


	for(int i = 0 ; i < semanas.size()-1 ; i++){
		vector<int> hoy;
		vector<int> proxima;
		tokenizar(semanas[i] , hoy);
		tokenizar(semanas[i+1] , proxima);

		if(mes != 12){
			if(hoy[0] <= ano && proxima[0] >= ano){

//cout << "Hoy: " << hoy[2] << " " << hoy[1] << " " << hoy[0] << endl;
//cout << "Proximo: " << proxima[2] << " " << proxima[1] << " " << proxima[0] << endl;
				if(hoy[2] <= dia && proxima[2] > dia && mes == hoy[1] && mes == proxima[1]){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}else if(hoy[2] >= dia && mes == hoy[1] && mes != proxima[1] && proxima[2] < dia){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}else if(hoy[2] <= dia && mes == hoy[1] && mes != proxima[1] && proxima[2] < dia){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}else if(hoy[2] >= dia && mes != hoy[1] && mes == proxima[1] && proxima[2] > dia){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}
			}

		}else{
			if(hoy[0] == ano && proxima[0] >= ano){

//cout << "Hoy: " << hoy[2] << " " << hoy[1] << " " << hoy[0] << endl;
//cout << "Proximo: " << proxima[2] << " " << proxima[1] << " " << proxima[0] << endl;
				if(hoy[2] <= dia && proxima[2] > dia && mes == hoy[1] && mes == proxima[1]){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}else if(hoy[2] >= dia && mes == hoy[1] && mes != proxima[1] && proxima[2] < dia){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}else if(hoy[2] <= dia && mes == hoy[1] && mes != proxima[1] && proxima[2] < dia){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}else if(hoy[2] >= dia && mes != hoy[1] && mes == proxima[1] && proxima[2] > dia){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}
			}	
		}
	}



	if(encontrado == false){
		cout << "NO se encontro la fecha" << endl;
		cout << dia << " " << mes << " " << ano << endl;
	}

	int contador = 0;
	int dia_termporal = dia_inicio;
	int mes_temporal = mes_inicio;
	int ano_temporal = ano_inicio;

//cout << dia_inicio << " " << dia_termino << endl;
	for(int i = 0 ; i < 7 ; i++){

		if(ano_temporal%4 == 0){
			meses[2] = 29;
		}else{
			meses[2] = 28;
		}


		if(dia_termporal == dia){
			break;
		}


		if(dia_inicio <= dia && dia > dia_termino){
//cout << dia_termporal << endl;

			if(meses[mes_temporal] == dia_termporal){
				dia_termporal = 1;	
				if(meses[mes_temporal] == 12){
					mes_temporal = 1;
					ano_temporal++;
				}else{
					mes_temporal++;
				}
			}else{

				dia_termporal++;
			}	

			contador++;

		}else if(dia_inicio >= dia && dia_termino > dia){

//cout << dia_termporal << endl;

			if(meses[mes_temporal] == dia_termporal){
				dia_termporal = 1;	
				if(meses[mes_temporal] == 12){
					mes_temporal = 1;
					ano_temporal++;
				}else{
					mes_temporal++;
				}
			}else{

				dia_termporal++;
			}	
			contador++;

		}else if(dia_inicio <= dia && dia_termino > dia){

//cout << dia_termporal << endl;

			if(meses[mes_temporal] == dia_termporal){
				dia_termporal = 1;	
				if(meses[mes_temporal] == 12){
					mes_temporal = 1;
					ano_temporal++;
				}else{
					mes_temporal++;
				}
			}else{

				dia_termporal++;
			}	
			contador++;

		}


	}
//cout << "FI: " << dia_inicio << "-" << mes_inicio << "-" << ano_inicio << " FT:" << dia_termino << "-" << mes_termino << "-" << ano_termino << " DB: " << dia << "-" << mes << "-" << ano << " SD: " << contador << " ID S: " << numero_guardado << endl;

//cout << nombre_archivo << endl;
	ifstream datosRuta(nombre_archivo);

	int contador2 = 0;
	int numeros = 17292*contador;
	//cout << "comienzo a guardar : " << numero_guardado << " " << numeros << endl;
 	while(!datosRuta.eof()){
		string linea;
		string linea2;
		string linea3;
		if(contador2 < 12){
			getline(datosRuta,linea);
			contador2++;

		}
		else{

			datosRuta >> linea;
			datosRuta >> linea2;
			datosRuta >> linea3;

			if(linea != "" && linea2 != "" && linea3 != "" ){
				float magnitud_explosion;
				magnitud_explosion = stof(linea3.c_str());
				//cout << linea << " " << linea2 << " " << magnitud_explosion << endl;
				m[numero_guardado][numeros] = magnitud_explosion;
				numeros++;
			}
		}
	}
	datosRuta.close();

}

void int_tostring(int dia, int mes, int ano, vector<string> &semanas){

	stringstream fecha;

	fecha << ano << '-' << mes << '-' << dia;	

	semanas.push_back(fecha.str());

}



void Copiar(float **a , float **m1 , int n){

	cout << n << endl;

	for(int i = 0 ; i < n ; i++){
		for(int j = 0 ; j < n ; j++){
			a[i][j] = m1[i][j];
			cout << j << endl;

		}
		//cout << i << endl;
	}

	cout << "sali bien" << endl;
}


void valoresPropios(float *valores, int maxIter , int n , float **a , float **p_normal){


	//cout << "kkkk" << endl;
	float CERO = 1e-8;
	float maximo; 
	float tolerancia;
	float sumsq;
	float x, y, z, c, s;
	int contador = 0;

	float *p;
	float *a_copia; 
	float *temporal; 
	float *resultado;
	//float **p_normal; 

	/*
	p_normal = (float **)malloc(sizeof(float)*n);

	for(int i = 0; i < n; i++) 
		p_normal[i]=(float *)malloc(sizeof(float)*n);
	*/

	float sizeA = sizeof(float) * (n * n);
	float sizeB = sizeof(float) * (n * n);
	float sizeC = sizeof(float) * (n * n);


	//cout << "lllll" << endl;
	p = (float *)malloc(sizeA);
	a_copia = (float *)malloc(sizeA);
	temporal = (float *)malloc(sizeB);
	resultado = (float *)malloc(sizeC);


	int k, l;
	/*
	float **a;

	a = (float **)malloc(sizeof(float)*n);

	for(int i = 0; i < n; i++) 
		a[i]=(float *)malloc(sizeof(float)*n);

	*/

	

	for(int i = 0 ; i < n*n ; i++){
		resultado[i] = 0.0;
	}

	//Matriz unidad
	for(int i = 0 ; i < n ; i++){
		resultado[i*n+i] = 1.0;
	}

	do{
		k = 0; 
		l = 1;
		maximo = absoluto(a[k][1]);
		for(int i = 0; i < n-1; i++){
			for(int j = i+1; j < n; j++){
				if(absoluto(a[i][j]) > maximo){
					k=i;        
					l=j;
					maximo = absoluto(a[i][j]);
				}
			}
		}

		sumsq = 0.0;

		for(int i = 0; i < n; i++){
			sumsq += a[i][i]*a[i][i];
		}
		tolerancia = 0.0001*sqrt(sumsq)/n;
		cout << maximo << " " << tolerancia << endl;
		if(maximo < tolerancia){
			break;
		}


		//calcula la matriz ortogonal de p
		//inicialmente es la matriz unidad

		for(int i = 0; i < n; i++){
    			for(int j = 0; j < n; j++){
    				p_normal[i][j] = 0.0;
    			}
			//cout << i << endl;
    		}
    
		//cout << "xxxxx" << endl;


		for(int i = 0; i < n; i++){
    			p_normal[i][i] = 1.0;
    		}

		y = a[k][k]-a[l][l];

		if(absoluto(y) < CERO){
			c = sin(PI/4); 
			s = sin(PI/4);
		}else{
			x = 2*a[k][l];
			z = sqrt(x*x+y*y);
			c = sqrt((z+y)/(2*z));
			s = signo(x/y)*sqrt((z-y)/(2*z));
		}


		//cout << "aca 100" << endl;

		p_normal[k][k] = c;
		p_normal[l][l] = c;
		p_normal[k][l] = s;
		p_normal[l][k] = -s;


		//cout << "aca 0" << endl;


		int contador_tras1 = 0;
		for(int i = 0 ; i < n ; i++){
			for(int j = 0 ; j < n ; j++){

				p[contador_tras1] = p_normal[i][j];
				contador_tras1++;
			}
		}
		
		//cout << "aca 1" << endl;

		//Traspuesta p
		int contador_tras = 0;
		for(int i = 0 ; i < n ; i++){
			for(int j = 0 ; j < n ; j++){

				temporal[contador_tras] = p[j*n+i];
				contador_tras++;
			}
		}


		contador_tras = 0;
		for(int i = 0 ; i < n ; i++){
			for(int j = 0 ; j < n ; j++){

				a_copia[contador_tras] = a[i][j];
				contador_tras++;
			}
		}


		//cout << "aca 2" << endl;


		float *deviceA = NULL;
		float *deviceB = NULL;
		float *deviceC = NULL;

		// Memoria en device
		cudaMalloc((void **)&deviceA, sizeA);
		cudaMalloc((void **)&deviceB, sizeB);
		cudaMalloc((void **)&deviceC, sizeC);

		cudaMemcpy(deviceA, a_copia, sizeA, cudaMemcpyHostToDevice);
		cudaMemcpy(deviceB, temporal, sizeB, cudaMemcpyHostToDevice);

		dim3 dimGrid((n - 1) / TILE_WIDTH + 1,
			(n - 1) / TILE_WIDTH + 1, 1);
		dim3 dimBlock(TILE_WIDTH, TILE_WIDTH, 1);

		// Hago a la función donde le envío las matrices y sus respectivo datos
		matrixMultiply<<<dimGrid, dimBlock>>>(deviceA, deviceB, deviceC, n,
			n, n, n,
			n, n);

		cudaMemcpy(temporal, deviceC, sizeC, cudaMemcpyDeviceToHost);

		
		cudaFree(deviceA);
		cudaFree(deviceB);
		cudaFree(deviceC);


		//*deviceA = NULL;
		//*deviceB = NULL;
		//*deviceC = NULL;

		// Memoria en device
		cudaMalloc((void **)&deviceA, sizeA);
		cudaMalloc((void **)&deviceB, sizeB);
		cudaMalloc((void **)&deviceC, sizeC);

		cudaMemcpy(deviceA, p, sizeA, cudaMemcpyHostToDevice);
		cudaMemcpy(deviceB, temporal, sizeB, cudaMemcpyHostToDevice);


		// Hago a la función donde le envío las matrices y sus respectivo datos
		matrixMultiply<<<dimGrid, dimBlock>>>(deviceA, deviceB, deviceC, n,
			n, n, n,
			n, n);

		cudaMemcpy(a_copia, deviceC, sizeC, cudaMemcpyDeviceToHost);


		cudaFree(deviceA);
		cudaFree(deviceB);
		cudaFree(deviceC);


		//Normal
		for(int i = 0 ; i < n ; i++){
			for(int j = 0 ; j < n ; j++){

				a[i][j] = a_copia[i*n+j];
			}
		}


		//Traspuesta p
		contador_tras = 0;
		for(int i = 0 ; i < n ; i++){
			for(int j = 0 ; j < n ; j++){

				temporal[contador_tras] = p[j*n+i];
				contador_tras++;
			}
		}

		//*deviceA = NULL;
		//*deviceB = NULL;
		//*deviceC = NULL;

		// Memoria en device
		cudaMalloc((void **)&deviceA, sizeA);
		cudaMalloc((void **)&deviceB, sizeB);
		cudaMalloc((void **)&deviceC, sizeC);

		cudaMemcpy(deviceA, temporal, sizeA, cudaMemcpyHostToDevice);
		cudaMemcpy(deviceB, resultado, sizeB, cudaMemcpyHostToDevice);


		// Hago a la función donde le envío las matrices y sus respectivo datos
		matrixMultiply<<<dimGrid, dimBlock>>>(deviceA, deviceB, deviceC, n,
			n, n, n,
			n, n);

		cudaMemcpy(resultado, deviceC, sizeC, cudaMemcpyDeviceToHost);

		cudaFree(deviceA);
		cudaFree(deviceB);
		cudaFree(deviceC);

		contador++;


	}while(contador < maxIter);

	if(contador == maxIter){
		cout << "Es demasiado impresiso se necesitan más iteraciones" << endl;
	}

	for(int i = 0; i < n; i++){
		valores[i]=(float)((a[i][i]*1000)/1000);
	}

	//vectores propios

	//Normal
	for(int i = 0 ; i < n ; i++){
		for(int j = 0 ; j < n ; j++){

			a[i][j] = resultado[i*n+j];
		}
	}

}


int signo(float x){
	return (x>0 ? 1 : -1);
}

float absoluto(float x){

	if(x < 0){
		x = x*-1;
	}

	return x;
}


