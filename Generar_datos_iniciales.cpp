#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <sstream>
#include <string>
#include <cstring>
#include <stdlib.h> 
#include <string>
#include <chrono>
#include <ctime>
#include <stdio.h>
#include <time.h>
using namespace std;

#define TILE_WIDTH 16

//Funcion para separar las fechas
void tokenizar(string fecha , vector<int> &numeros);

//Funcion para leer los archivos
void leer_archivo(string nombre_archivo, float **m, vector<string> &semanas , int dia , int mes , int ano);

//Funcion para pasar de int a string
void int_tostring(int dia, int mes, int ano, vector<string> &semanas);

//Funcion para calcular todas las semanas entre 2 fechas
void calcular_semanas(vector<string> &semanas , int dia , int mes , int ano , int dia_termino , int mes_termino, int ano_termino );


int signo(float x);
float **producto(float **a ,float **b, int n);
float **traspuesta(float **a , int n);
float **Copiar(float **m1 , int n);
float **valoresPropios(float *valores, int maxIter , int n , float **m1);
float absoluto(float x);


int main(){

	vector<string> semanas;

	calcular_semanas(semanas, 22 , 4 , 2013 , 28 , 5 , 2016);


	float *hostA;
	float *valor_propio;  
	float *hostB; 
	float *hostC; 
	float *deviceA = NULL;
	float *deviceB = NULL;
	float *deviceC = NULL;
	int numrowA;
	int numcolsA;
	int numrowA_Traspuesta;
	int numcolsA_Traspuesta;


	numrowA = semanas.size()-1;
	numcolsA = (17292*7);

	numrowA_Traspuesta = (17292*7);
	numcolsA_Traspuesta = semanas.size()-1;

	int numCrows = numrowA;
	int numCColumns = numcolsA_Traspuesta;

	float sizeA = sizeof(float) * (numrowA * numcolsA);
	float sizeB = sizeof(float) * (numrowA_Traspuesta * numcolsA_Traspuesta);
	float sizeC = sizeof(float) * numCrows * numCColumns;
	float sizeD = sizeof(float) * numrowA;


	hostA = (float *)malloc(sizeA);
	valor_propio = (float *)malloc(sizeD);
	hostB = (float *)malloc(sizeB);
	hostC = (float *)malloc(sizeC);

	float **m;

	m = new float * [numrowA];

	for(int i = 0; i < numrowA; i++)
		m[i] = new float [numcolsA];


	for(int i = 0 ; i < numrowA ; i++){
		for(int j = 0 ; j < numcolsA ; j++){
			m[i][j] = 0.0;
		}
	}


	vector<string> nombre_archivo;

	string archivo;

	ifstream datosRuta("listado.txt");

	int contador2 = 0;
	while(!datosRuta.eof()){

		string linea;
		getline(datosRuta,linea);
		nombre_archivo.push_back(linea);
	}
	datosRuta.close();


	for(int i = 0 ; i < nombre_archivo.size() ; i++){
		string str2 = nombre_archivo[i].substr (8,10);
		vector<int> fecha;
		tokenizar(str2 , fecha);
		string fichero_inicial = "Archivos/";	
		string nombre_final = fichero_inicial+nombre_archivo[i];
		leer_archivo(nombre_final , m , semanas , fecha[2] , fecha[1] , fecha[0]);
	}

	ofstream salida("datos_iniciales");

	for(int i = 0 ; i < numrowA ; i++){
		for(int j = 0 ; j < numcolsA ; j++){
			salida << m[i][j] << " ";
		}
		salida << endl;
	}



	return 0;
}


void calcular_semanas(vector<string> &semanas , int dia , int mes , int ano , int dia_termino , int mes_termino, int ano_termino ){

	map<int,int> meses;
	meses[1] = 31;
	meses[2] = 28;
	meses[3] = 31;
	meses[4] = 30;
	meses[5] = 31;
	meses[6] = 30;
	meses[7] = 31;
	meses[8] = 31;
	meses[9] = 30;
	meses[10] = 31;
	meses[11] = 30;
	meses[12] = 31;

	int contador_semana = 0;

	int_tostring(dia,mes,ano,semanas);

	while(mes < mes_termino+1 || ano != ano_termino){

		dia = dia+7;

		if(mes == 2){
			if(ano%4 == 0){
				meses[mes] = 29;
			}else{
				meses[mes] = 28;
			}
		}

		if(meses[mes] > dia){
			contador_semana++;

		}
		else if(meses[mes] < dia){
			contador_semana++;
			dia = dia-meses[mes];

			if(mes == 12){
				mes = 1;
				ano++;
			}else{
				mes++;
			}
		}

		if(mes < mes_termino+1 || ano != ano_termino){
			int_tostring(dia,mes,ano,semanas);
		}

	}

}

void tokenizar(string fecha , vector<int> &numeros){

	string palabra;
	palabra = "";

	for(int i = 0; i < fecha.size() ; i++){

		if(fecha[i] == '-'){
			int num = atoi(palabra.c_str());
			numeros.push_back(num);
			palabra = "";
		}else{
			palabra += fecha[i];
		}

	}
	int num = atoi(palabra.c_str());
	numeros.push_back(num);

}


void leer_archivo(string nombre_archivo, float **m ,vector<string> &semanas, int dia , int mes , int ano){

	map<int,int> meses;
	meses[1] = 31;
	meses[2] = 28;
	meses[3] = 31;
	meses[4] = 30;
	meses[5] = 31;
	meses[6] = 30;
	meses[7] = 31;
	meses[8] = 31;
	meses[9] = 30;
	meses[10] = 31;
	meses[11] = 30;
	meses[12] = 31;


//obtengo donde tiene que ir
	bool encontrado = false;
	int numero_guardado;

	int dia_inicio;
	int mes_inicio;
	int ano_inicio;

	int dia_termino;
	int mes_termino;
	int ano_termino;


	for(int i = 0 ; i < semanas.size()-1 ; i++){
		vector<int> hoy;
		vector<int> proxima;
		tokenizar(semanas[i] , hoy);
		tokenizar(semanas[i+1] , proxima);

		if(mes != 12){
			if(hoy[0] <= ano && proxima[0] >= ano){

//cout << "Hoy: " << hoy[2] << " " << hoy[1] << " " << hoy[0] << endl;
//cout << "Proximo: " << proxima[2] << " " << proxima[1] << " " << proxima[0] << endl;
				if(hoy[2] <= dia && proxima[2] > dia && mes == hoy[1] && mes == proxima[1]){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}else if(hoy[2] >= dia && mes == hoy[1] && mes != proxima[1] && proxima[2] < dia){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}else if(hoy[2] <= dia && mes == hoy[1] && mes != proxima[1] && proxima[2] < dia){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}else if(hoy[2] >= dia && mes != hoy[1] && mes == proxima[1] && proxima[2] > dia){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}
			}

		}else{
			if(hoy[0] == ano && proxima[0] >= ano){

//cout << "Hoy: " << hoy[2] << " " << hoy[1] << " " << hoy[0] << endl;
//cout << "Proximo: " << proxima[2] << " " << proxima[1] << " " << proxima[0] << endl;
				if(hoy[2] <= dia && proxima[2] > dia && mes == hoy[1] && mes == proxima[1]){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}else if(hoy[2] >= dia && mes == hoy[1] && mes != proxima[1] && proxima[2] < dia){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}else if(hoy[2] <= dia && mes == hoy[1] && mes != proxima[1] && proxima[2] < dia){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}else if(hoy[2] >= dia && mes != hoy[1] && mes == proxima[1] && proxima[2] > dia){
//cout << "fecha es: " << ano << "-" << mes << "-" << dia << endl;
//cout << "lo encontre: " << hoy[0] << "-" << hoy[1] << "-" << hoy[2] << " " << proxima[0] << "-" << proxima[1] << "-" << proxima[2] << endl;
					numero_guardado = i;
					ano_inicio = hoy[0];
					ano_termino = proxima[0];
					mes_inicio = hoy[1];
					mes_termino = proxima[1];
					dia_inicio = hoy[2];
					dia_termino = proxima[2];
					encontrado = true;
					break;

				}
			}	
		}
	}



	if(encontrado == false){
		cout << "NO se encontro la fecha" << endl;
		cout << dia << " " << mes << " " << ano << endl;
	}

	int contador = 0;
	int dia_termporal = dia_inicio;
	int mes_temporal = mes_inicio;
	int ano_temporal = ano_inicio;

//cout << dia_inicio << " " << dia_termino << endl;
	for(int i = 0 ; i < 7 ; i++){

		if(ano_temporal%4 == 0){
			meses[2] = 29;
		}else{
			meses[2] = 28;
		}


		if(dia_termporal == dia){
			break;
		}


		if(dia_inicio <= dia && dia > dia_termino){
//cout << dia_termporal << endl;

			if(meses[mes_temporal] == dia_termporal){
				dia_termporal = 1;	
				if(meses[mes_temporal] == 12){
					mes_temporal = 1;
					ano_temporal++;
				}else{
					mes_temporal++;
				}
			}else{

				dia_termporal++;
			}	

			contador++;

		}else if(dia_inicio >= dia && dia_termino > dia){

//cout << dia_termporal << endl;

			if(meses[mes_temporal] == dia_termporal){
				dia_termporal = 1;	
				if(meses[mes_temporal] == 12){
					mes_temporal = 1;
					ano_temporal++;
				}else{
					mes_temporal++;
				}
			}else{

				dia_termporal++;
			}	
			contador++;

		}else if(dia_inicio <= dia && dia_termino > dia){

//cout << dia_termporal << endl;

			if(meses[mes_temporal] == dia_termporal){
				dia_termporal = 1;	
				if(meses[mes_temporal] == 12){
					mes_temporal = 1;
					ano_temporal++;
				}else{
					mes_temporal++;
				}
			}else{

				dia_termporal++;
			}	
			contador++;

		}


	}
//cout << "FI: " << dia_inicio << "-" << mes_inicio << "-" << ano_inicio << " FT:" << dia_termino << "-" << mes_termino << "-" << ano_termino << " DB: " << dia << "-" << mes << "-" << ano << " SD: " << contador << " ID S: " << numero_guardado << endl;

//cout << nombre_archivo << endl;
	ifstream datosRuta(nombre_archivo);

	int contador2 = 0;
	int numeros = 17292*contador;
	while(!datosRuta.eof()){
		string linea;
		string linea2;
		string linea3;
		if(contador2 < 12){
			getline(datosRuta,linea);
			contador2++;

		}
		else{

			datosRuta >> linea;
			datosRuta >> linea2;
			datosRuta >> linea3;

			if(linea != "" && linea2 != "" && linea3 != "" ){
				float magnitud_explosion;
				magnitud_explosion = stof(linea3.c_str());
//cout << linea << " " << linea2 << " " << magnitud_explosion << endl;
				m[numero_guardado][numeros] = magnitud_explosion;
				numeros++;
			}
		}
	}
	datosRuta.close();

}

void int_tostring(int dia, int mes, int ano, vector<string> &semanas){

	stringstream fecha;

	fecha << ano << '-' << mes << '-' << dia;	

	semanas.push_back(fecha.str());

}


