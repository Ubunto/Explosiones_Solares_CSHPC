from __future__ import print_function

# $example on$
from numpy import array
import numpy as np
from math import sqrt
from time import time, strftime
# $example off$

from pyspark import SparkContext
# $example on$
from pyspark.mllib.clustering import KMeans, KMeansModel
# $example off$

def parseDataset(lines):
    rdd = lines.map(lambda line: array([float(x) for x in line.split(' ')]))
    return rdd

def generate_probabilities(points, k, model, count_lines):
    probabilities = np.zeros(k)
    points = points.map(lambda point: (model.predict(point), 1.0)) \
                    .reduceByKey(lambda p1, p2: p1 + p2) \
                    .sortByKey() \
                    .collect()

    for p in points:
		probabilities[p[0]] = float(p[1]) / count_lines
    return probabilities


if __name__ == "__main__":

    sc = SparkContext(appName="KMeansExample") # SparkContext
    sameModel = KMeansModel.load(sc, "hdfs://masterNode:9000/user/spark/output/kmeans_felipe/KMeansModel")
	# Load and parse the data
    predict_data = sc.textFile("hdfs://masterNode:9000/user/spark/dataset_k_means/Pruebas_2013_2016.txt")
	#parsedData = data.map(lambda line: array([float(x) for x in line.split(' ')]))

    start = time()
	# Predicting
    points = parseDataset(predict_data)
    count_lines = float(len(points.collect()))
    probabilities = generate_probabilities(points, 10, sameModel, count_lines)
    print("Prob: ", probabilities)

    end = time()
    elapsed_time = end - start
	
    sc.stop()
	# Save and load model
	#clusters.save(sc, "hdfs://masterNode:9000/user/spark/output/kmeans_felipe/KMeansModel")
	#sameModel = KMeansModel.load(sc, "hdfs://masterNode:9000/user/spark/output/kmeans_felipe/KMeansModel")

