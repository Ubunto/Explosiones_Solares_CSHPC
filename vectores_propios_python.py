import pandas as pd
import numpy as np
import scipy as sp
import scipy.sparse as sparse
from scipy.sparse.linalg import svds, eigs, eigsh
from sklearn.metrics.pairwise import cosine_similarity

import sympy as syp
syp.init_printing()


def myformat(x):
    return ('%.5f' % x).rstrip('0').rstrip('.')


if __name__ == '__main__':
	"""
	A = np.array([[1, 5, 0, 5, 4], 
		[5, 4, 4, 3, 2],
		[0, 4, 0, 0, 5],
		[4, 4, 1, 4, 0],
		[0, 4, 3, 5, 0],
		[2, 4, 3, 5, 3]])

	A = A.astype(float)
	syp.Matrix(A)

	C = np.dot(A.T, A)

	np.linalg.eig(C)

	U, S, Vt = sp.linalg.svd(A)

	print("Matriz Traspuesta")
	print(C)
	print("")

	print("Valores Propios")
	print(S)
	print("")

	print("Vectores Propios")
	print(U)
	print("")
	"""

	A = np.zeros((162,121044))
	A = A.astype(float)

	print("Comenze a leer el archivo")
	archivo = open("datos_iniciales.txt", "r")
	contador = 0	
	while True:
 		linea = archivo.readline()
 		if not linea: break
		else: 
			keyword_ratings = linea.split(" ")
						
			for i in range(len(keyword_ratings)-1):
				A[contador][i] = float(keyword_ratings[i])/5500.00;
				#print isfloat(keyword_ratings[i])
		contador = contador+1	
	archivo.close

	print("Traspuesta")
	C = np.dot(A, A.T)

	
	print("Escribo la matriz C")
	f=open("Matriz_C.txt","w")
	for x in range(len(C)):
		for y in range(len(C)):
			f.write(str(C[x][y]))
			f.write(" ")	
		f.write("\n")
	f.close()
	
	print("Comenze a calcular")
	

	#np.linalg.eig(C)

	U, S, Vt = sp.linalg.svd(C)
	#print("Matriz Traspuesta")
	#print(C)
	#print("")

	#print("Valores Propios")
	#print(S)
	#print("")

	print("Termine de calcular")

    #Paso los valores de U a positivo
	for x in range(len(U)):
		for y in range(len(U)):
			if(U[x][y] < 0):
				U[x][y] = U[x][y]*(-1)

	'''
	print("Escribo los Valores Propios S")
	f=open("Valores_Propios_S.txt","w")
	for x in range(len(S)):
		f.write(str(S[x]))
		f.write(" ")	
	f.write("\n")
	f.close()
	'''

	#print("Vectores Propios")
	#print(U)
	#print("")

	'''
	print("Escribo los Vectores Propios U")
	f=open("Matriz_U.txt","w")
	for x in range(len(U)):
		for y in range(len(U)):
			f.write(str(U[x][y]))
			f.write(" ")	
		f.write("\n")
	f.close()
	'''
	contador_repeticion = 0
	tomar_columna = 0
	dato = 0	
	for x in range(len(S)-1):
			
		if(contador_repeticion == 10):
			tomar_columna = x
			dato = S[x]
			break		

		if(int(S[x]) == int(S[x+1])):		
			contador_repeticion = contador_repeticion+1
		else:
			contador_repeticion = 0

	
	print(tomar_columna)	
	print(dato)

	"""
	print("Escribo los Valores Propios Escogidos S")
	f=open("Valores_Propios_Escogidos_S.txt","w")
	for x in range(tomar_columna):
		f.write(str(S[x]))
		f.write(" ")	
	f.write("\n")
	f.close()


	print("Escribo los Vectores Propios Escogidos de U")
	f=open("Matriz_vectores_Escogidos_U.txt","w")
	for x in range(len(U)):
		for y in range(tomar_columna):
			f.write(str((U[x][y])))
			f.write(" ")	
		f.write("\n")
	f.close()
	"""

	print("Escribo los Vectores Propios Escogidos de U")
	f=open("kmeans_data.txt","w")
	for x in range(len(U)):
		for y in range(tomar_columna):
			if(y < tomar_columna-1):						
				f.write(str((U[x][y])))
				f.write(" ")
			else:
				f.write(str((U[x][y])))				
		f.write("\n")
	f.close()



	print("Escribo los Vectores Propios Escogidos de U")
	f=open("pic_data.txt","w")
	for x in range(len(U)):
		for y in range(tomar_columna):	
			if(y < tomar_columna-1):
				f.write(str(x))
				f.write(" ")
				f.write(str(y))
				f.write(" ")						
				f.write(str((U[x][y])))
				f.write("\n")			
	f.close()


	print("Escribo los Vectores Propios Escogidos de U")
	f=open("streaming_kmeans_data_test.txt","w")
	for x in range(len(U)):
		f.write("(")
		f.write(str(float(x+1)))
		f.write(")")
		f.write(", ")
		f.write("[")
		for y in range(tomar_columna):	
			if(y < tomar_columna-1):					
				f.write(str((U[x][y])))
				f.write(", ")
			else:
				f.write(str((U[x][y])))
				f.write("]")
				f.write("\n")			
	f.close()


	"""
	print("Escribo el archivo final")
	f=open("Similitudes.txt","w")
	for x in range(len(U)):
		f.write(str(x+1))
		f.write(str(" "))	
		for y in range(tomar_columna):
			f.write(str(y+1))
			f.write(str("|"))				
			f.write(str(U[x][y]))
			if(y != (tomar_columna-1)):				
				f.write(str(";"))	
		f.write("\n")
	f.close()
	"""
	"""
	print("Escribo el archivo final raitings")
	f=open("ratings_nuevo.csv","w")
	for x in range(len(U)):	
		for y in range(tomar_columna):
			f.write(str(x+1))
			f.write(str(","))			
			f.write(str(y+1))
			f.write(str(","))				
			f.write(str((U[x][y])))
			f.write(str(","))
			f.write(str(10))			
			f.write("\n")
	f.close()

	print("Escribo el archivo final movies")
	f=open("movies_nuevo.csv","w")
	for y in range(tomar_columna):			
		f.write(str(y+1))
		f.write(str(","))				
		f.write(str("Nombre"))
		f.write(str(","))				
		f.write(str("Categorias"))				
		f.write("\n")
	f.close()
	"""
